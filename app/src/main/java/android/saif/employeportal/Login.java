package android.saif.employeportal;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;

public class Login extends AppCompatActivity {

    private View recoverwindow;
    private View card;
    private View recoverAccount;
    private View Recovery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        recoverwindow=findViewById(R.id.recovercard);
        card=findViewById(R.id.card);
        recoverAccount=findViewById(R.id.email);
        Recovery=findViewById(R.id.recover);
        recoverAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recoverwindow.setVisibility(View.GONE);
                card.setVisibility(View.VISIBLE);
            }
        });
        Recovery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                card.setVisibility(View.GONE);
                recoverwindow.setVisibility(View.VISIBLE);

            }
        });

    }

}
